/* Auxiliar libraries */
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

/* Models */
const Usuario = require('../../models/user')


const { OAuth2Client } = require('google-auth-library')
const client = new OAuth2Client(process.env.CLIENT_ID)


/* Funciones Auxiliares */
async function verify(token) {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID,
  })
  const payload = ticket.getPayload()

  return payload
}


module.exports = {
  login: async (req, res) =>{
    
    try {
      const {email, password} = req.body
      const userFound = await Usuario.findOne({email: email})  
      
      if(!userFound) {
        return res.status(400).json({
          ok: false,
          error: {
            message: "(Usuario) o contraseña incorrectas"
          }
        })
      }
      
      if(!bcrypt.compareSync(password, userFound.password)){
        return res.status(400).json({
          ok: false,
          error: {
            message: "Usuario o (contraseña) incorrectas."
          }
        })
      }

      let token = jwt.sign(
        {usuario: userFound}, 
        process.env.SEED, 
        {expiresIn: process.env.CADUCIDAD_TOKEN}
      )

      res.json({
        ok: true,
        usuario: userFound,
        token
      })

    } catch (error) {
      console.log(error) 
      res.status(500).json({
        ok: false,
        error
      })
    }
  },

  loginWithGoogle: async (req, res) => {
    const { idtoken: token } = req.body

    try {
      const googleUser = await verify(token).catch((e) => {
        return res.status(403).json({
          ok: false,
          err: e
        })
      })

      const userFound = await Usuario.findOne({email: googleUser.email})

      if (userFound) {
        if (userFound.google === false) {
          res.status(400).json({
            ok: false,
            err: {
              message: "Debe de usar su autenticación normal"
            }
          })
        } else {
          const token = jwt.sign(
            { usuario: userFound, },
            process.env.SEED,
            { expiresIn: process.env.CADUCIDAD_TOKEN }
          );

          return res.json({
            ok: true,
            usuario: userFound,
            token
          })
        }

      } else {
        const usuario = new Usuario();

        usuario.nombre = googleUser.name
        usuario.email = googleUser.email
        usuario.img = googleUser.picture
        usuario.google = true
        usuario.password = "123"

        const userSaved = await usuario.save()

        const token = jwt.sign(
          { usuario: userSaved, },
          process.env.SEED,
          { expiresIn: process.env.CADUCIDAD_TOKEN }
        );

        return res.json({
          ok: true,
          usuario: userSaved,
          token
        })
      }

    } catch (error) {
      console.log(error)
      res.status(500).json({
        ok: false,
        error
      })
    }

    res.json({
      token,
    })
  }
}