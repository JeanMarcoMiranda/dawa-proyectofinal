/* Main libraries */
const router = require('express').Router()

/* My modules */
const { login, loginWithGoogle } = require('./loginController')


router.route('/')
  .post(login)

router.route('/google')
  .post(loginWithGoogle)

module.exports = router