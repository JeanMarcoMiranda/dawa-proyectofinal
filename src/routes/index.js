const app = require('express')();
const bodyParser = require('body-parser')
const morgan = require('morgan');


//Middlewares
app.use(morgan('dev'))

app.use(bodyParser.urlencoded({
  extended: false
}));


//Connecting our routes
app.use('/auth', require('./Login/login.routes'))
app.use('/api/users', require('./User/user.routes'))



module.exports = app