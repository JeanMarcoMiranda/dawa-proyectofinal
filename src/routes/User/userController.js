const bcrypt = require('bcrypt')
const Usuario = require('../../models/user')
const jwt = require('jsonwebtoken')

module.exports = {

  createUser: async (req, res) => {
    try{
      const userToSave = {
        nombre: req.body.nombre,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
      }

      if(req.file){
        userToSave.img = req.file.path
      }

      const userSaved = new Usuario(userToSave)
      await userSaved.save()
  
      res.json({
        ok: true,
        user: userSaved
      })
      
    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
      })
    }
  },

  getAllUsers: async (req, res) => {
    try{
      const token = req.get("token")
      let data
      if(token){
        const decoded = jwt.verify(token, process.env.SEED)

        data = decoded.usuario
      } else {
        data = await Usuario.find({})
      }

      res.json({
        ok: true,
        usuarios : data
      })
    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
        error,
      })
    }
  },

  getOneUser: async (req, res) => {
    try{
      const {userId} = req.params
      console.log(userId)

      const userFound = await Usuario.findById(userId)
      
      res.json({
        ok: true,
        usuario : userFound
      })

    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
        error,
      })
    }
  },


  updateUser: async (req, res) => {
    try{
      const userToSave = {
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password,
      }

      if(req.file){
        userToSave.img = `${req.file.filename}`
      }

      const {userId} = req.params

      const userUpdated = await Usuario.findByIdAndUpdate(userId, userToSave, {new: true})
      res.status(200).json({
        ok: true,
        message: 'User updated.',
        newUserData: userUpdated
      })

    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
        error
      })
    }
  },

  deleteUser: async (req, res) => {
    try{
      const {userId} = req.params
      console.log(userId)
      await Usuario.findByIdAndDelete(userId)
      res.json({
        ok: true,
        message: 'User Deleted Successfully'
      })

    }catch(error){
      console.error(error)
      res.status(400).json({
        ok: false,
        error
      })
    }
  }
}