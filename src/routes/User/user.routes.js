const router = require('express').Router();
const multer = require('multer');
const fs = require('fs')
const path = require('path')

/* My models */
const Usuario = require('../../models/user')

//File storage configuration functions
const storageConfig = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads/')
    
  },

  filename: async (req, file, cb) => {
    if(req.params.userId){
      const {userId : id} = req.params
      console.log(file.originalname)
      const nameSplitted = file.originalname.split(".")
      const extension = nameSplitted[nameSplitted.length - 1]

      const user =  await Usuario.findById(id)


      const fileName = `${id}-${new Date().getMilliseconds()}.${extension}`;
      const pathImage = path.resolve(__dirname, `../../../uploads/${user.img}`)

      if(fs.existsSync(pathImage)){
        fs.unlinkSync(pathImage)
      }

      cb(null, fileName)

    } else {
      cb(null, file.originalname)
    }
  }
})

const fileFilterConfig = (req, file, cb) => {
  const mimetypesAllowed = [
    'image/png', 
    'image/jpg', 
    'image/gif', 
    'image/jpeg'
  ]

  if(mimetypesAllowed.includes(file.mimetype)){
    cb(null, true)
  } else {
    cb(
      new Error(`Archivos ${file.mimetype} no estan permitidos`), 
      false
    )
  }
}


//Setting our ulpoad
const upload = multer(
  {
    storage: storageConfig,
    fileFilter: fileFilterConfig
  }
);

const {
  createUser,
  getAllUsers,
  getOneUser,
  updateUser,
  deleteUser,
  getUserByToken
} = require('./userController')

router.route('/')
  .get(getAllUsers)
  .post(upload.single('userImage'), createUser)

router.route('/:userId')
  .get(getOneUser)
  .put(upload.single('userImage'), updateUser)
  .delete(deleteUser)

module.exports = router