const mongoose = require('mongoose')
require('./config/databaseConfig')

mongoose.connect(
  process.env.URLDB,
  {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true}) 
  .then(db => console.log('DB is successfully connected'))
  .catch(err => console.error(err))

module.exports = mongoose