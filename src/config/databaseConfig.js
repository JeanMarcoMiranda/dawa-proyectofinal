/* Configuracion de puerto */
process.env.PORT = process.env.PORT || 3000


/* Entorno */
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'


/* Configuracion de la base de datos */
let urlDB
if(process.env.NODE_ENV === 'dev'){
  urlDB = 'mongodb://localhost:27017/MERN'
}else{
  urlDB = 'mongodb+srv://jmiranda:B6or5m9ioc5blWUB@cluster0-mwu7h.mongodb.net/MERN'
  
}
process.env.URLDB = urlDB


/* Vencimiento de Token /Un Mes/ */  
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30


/* SEED de autenticación */
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo'

/* Google client ID */
process.env.CLIENT_ID = process.env.CLIENT_ID || '406073188678-67k8umgs9eifko5kfefu1ppi5d3v8mpq.apps.googleusercontent.com'