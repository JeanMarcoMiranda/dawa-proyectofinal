const express = require('express')
const path = require('path')
const {mongoose} = require('./database')
require('./config/databaseConfig')

const { Usuarios } = require('./classes/usuarios')
const { crearMensaje } = require('./utilidades/utilidades')
const socketIO = require('socket.io')
const http = require('http')
const app = express()
const server = http.createServer(app)

// Socket configuration instance
const io = socketIO(server)
const usuarios = new Usuarios()

io.on("connection", socket => {

  socket.on("entrarChat", ({name, room}, callback) => {
    
    if (!name || !room) {
      return callback({
          error: true,
          mensaje: 'El nombre y la sala son necesarios',
      })
    }

    socket.join(room)
    usuarios.agregarPersona(socket.id, name, room)

    socket.broadcast.to(room)
      .emit('listarPersonasSala', usuarios.getPersonasPorSala(room))
    console.log("Usuarios en la sala ", usuarios.getPersonasPorSala(room))

    socket.broadcast.to(room)
      .emit('crearMensaje', crearMensaje('Admin', `${name} se unió`))

  })

  socket.on('crearMensaje', ({message, room}) => {

    let persona = usuarios.getPersona(socket.id)
    let mensaje = crearMensaje(persona.nombre, message)

    io.to(room)
      .emit('crearMensaje', mensaje)

  });

  socket.on("disconnect", () => {
    console.log("Desconectando")
    let persona = usuarios.borrarPersona(socket.id)

    console.log("Estos son los usuarios", usuarios.getPersonas(socket.id))
    if(persona){
      console.log(persona.nombre + ' se volvió paraguayo')
      let mensaje = crearMensaje('Admin', `${persona.nombre} se volvió paraguayo`)
      socket.broadcast.to(persona.sala)
        .emit('crearMensaje', mensaje)
    }
  })

})

// =============================


app.use(express.json())

//Settingsclie
app.set('port', process.env.PORT)


//Importing our routes
app.use(require('./routes'))


//Static files
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static('uploads'))


//Starting server
server.listen(app.get('port'), () => {
  console.log(`Server listening on port ${app.get('port')}`)
})

