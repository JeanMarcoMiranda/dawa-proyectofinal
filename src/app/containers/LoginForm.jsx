import React, { useState } from 'react'
import { connect } from 'react-redux'
import { setLogedIn, setUserLoged } from '../actions'

import { useForm } from 'react-hook-form'
import axios from 'axios'
import { GoogleLogin } from 'react-google-login'

const Login = (props) => {

  const [emailIsFocus, setEmailIsFocus] = useState(false)
  const [passwordIsFocus, setPasswordIsFocus] = useState(false)
  const { register, handleSubmit, errors } = useForm()

  const handleEmailFocus = () => {
    setEmailIsFocus(!emailIsFocus)
  }
  const handlePasswordFocus = () => {
    setPasswordIsFocus(!passwordIsFocus)
  }


  const setDataToLogin = data => {
    axios.post('/auth/', data)
      .then(res => {
        const {token, usuario} = res.data
        localStorage.setItem('token', token)
        props.setLogedIn(token)
        props.setUserLoged(usuario)
        props.history.push("/list")
      })
      .catch(err => {
        alert("Usuario no encontrado, vuelva a intentarlo.")
      })
  }

  const responseGoogle = response => {
    const tokenId = response.tokenId
    axios.post('/auth/google/', { idtoken: tokenId })
      .then(res => {
        console.log(res.data)
        const {token, usuario} = res.data
        localStorage.setItem('token', token)
        props.setLogedIn(token)
        props.setUserLoged(usuario)
        props.history.push("/list")
      })
      .catch(err => {
        alert("Usuario no encontrado, vuelva a intentarlo.")
      })
    /* console.log(response) */
  }


  return (
    <div className="container align-self-baseline h-75">
      <div className="card w-50 h-100 mx-auto">
        <div className="card-body p-4">
          <figure className="d-flex justify-content-center mb-2">
            <img width="130" height="130" src="https://www.krama.es/images/logo-react-native.svg" />
          </figure>
          <form onSubmit={handleSubmit(setDataToLogin)}>
            <div className="form-group py-1">
              <label className="mt-3"><h5>Email</h5></label>
              <input
                type="text"
                name="email"
                className={`w-100 p-3 border ${(emailIsFocus) ? "border-primary" : "border-dark"}`}
                onFocus={handleEmailFocus}
                onBlur={handleEmailFocus}
                ref={register({ required: true, pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ })} />
              <span
                role="alert"
                id="error-email-required"
                style={{
                  display: errors.email && errors.email.type === "required"
                    ? "block"
                    : "none",
                  color: '#EF7676'
                }}
              >
                El campo email es necesario
              </span>
              <span
                role="alert"
                style={{
                  display: errors.email && errors.email.type === "pattern"
                    ? "block"
                    : "none",
                  color: '#EF7676'
                }}
              >
                Email ingresado invalido, pruebe con otro.
              </span>
            </div>


            <div className="form-group py-1">
              <label><h5>Contraseña</h5></label>
              <input
                type="password"
                name="password"
                className={`w-100 p-3 border ${(passwordIsFocus) ? "border-primary" : "border-dark"}`}
                onFocus={handlePasswordFocus}
                onBlur={handlePasswordFocus}
                ref={register({ required: true, minLength: 8, })} />
              <span
                role="alert"
                id="error-password-required"
                style={{
                  display: errors.password && errors.password.type === "required"
                    ? "block"
                    : "none",
                  color: '#EF7676'
                }}
              >
                La contraseña es necesaria
              </span>
              <span
                role="alert"
                id="error-email-required"
                style={{
                  display: errors.password && errors.password.type === "minLength"
                    ? "block"
                    : "none",
                  color: '#EF7676'
                }}
              >
                Este campo debe tener un minimo de 8 caracteres.
              </span>
            </div>
            <div className="mt-3">
              <button type="submit" className="btn btn-success py-3 w-100 rounded"><h5 className="my-auto">Ingresar</h5></button>
            </div>
            <div className="mt-3">
              <GoogleLogin
                clientId="406073188678-67k8umgs9eifko5kfefu1ppi5d3v8mpq.apps.googleusercontent.com"
                buttonText="Ingresar con Google"
                onSuccess={responseGoogle}
                onFailure={() => console.log("error")}
                className="w-100 btn rounded py-2 d-flex justify-content-center h3"
                /* cookiePolicy={'single_host_origin'} */
              />
            </div>
          </form>
        </div>
        <div className="card-footer text-muted">
          <div className="d-flex justify-content-center my-2">
            <h5>¿Aún no tienes una cuenta?</h5>
          </div>
          <div>
            <button type="submit" className="btn btn-primary py-3 w-100 mb-3">
              <h5 className="my-auto">
                Registrarse
                </h5>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapDispatchToProps = {
  setLogedIn,
  setUserLoged
}

export default connect(null, mapDispatchToProps)(Login)