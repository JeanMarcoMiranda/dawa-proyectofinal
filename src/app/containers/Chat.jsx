import React, { useState, useEffect } from 'react'

// Third party libraries
import queryString from 'query-string'
import io from 'socket.io-client'


//My COmponents
import MessageList from '../components/MessageList.jsx'


let socket;

const Chat = ({ location }) => {

   const [room, setRoom] = useState('')
   const [name, setName] = useState('')
   const [message, setMessage] = useState('')
   const [messages, setMessages] = useState([])

   const ENDPOINT = 'localhost:3000'

   useEffect(() => {
      const { name, room } = queryString.parse(location.search)
      setName(name)
      setRoom(room)

      socket = io(ENDPOINT)

      socket.emit('entrarChat', { name, room }, ({ error, mensaje }) => {
         alert(`Error: ${error}`)
         console.log(mensaje)
      })

      return () => {
         console.log(socket);
         socket.emit("disconnect")
         socket.off()
      }

   }, [ENDPOINT, location.search])

   useEffect(() => {
      /* Socket messagge */
      console.log(message)
      socket.on('crearMensaje', mensaje => {
         console.log('Servidor:', mensaje)
         setMessages(messages => [...messages, mensaje])
      })

      /* Socket userlist */
      socket.on('listarPersonasSala', personas => {
         console.log(personas);
         //Aqui funcion o codigo para mostrar usuarios en lista
      })

   }, [])

   const sendMessage = (event) => {
      event.preventDefault();
      console.log('Send Message: ', message)
      if (message) {
         //console.log("If send Message")
         socket.emit('crearMensaje', { message, room }, () => setMessage(''));
      }
   }

   const salir = () => {
      console.log("Salir")
      window.location.href = '/'
   }


   return (
      <div className="containerChat">
         {/* <h1 className="text-center">CHAT </h1>*/}
         <div className="messaging">
            <div className="inbox_msg">
               <div className="inbox_people">
                  <div className="headind_srch">
                     <div className="recent_heading">
                        <h4>Usuarios</h4>
                     </div>
                     <div className="salir">
                        <button onClick={salir} className="btn btn-danger">Desconectarse</button>
                     </div>
                  </div>
                  {/*Lista Usuarios*/}
                  <div className="inbox_chat">
                     <div className="chat_list active_chat">
                     </div>
                  </div>
                  {/*Fin lista Usuarios*/}
               </div>
               {/* Mensajes - chat */}
               <div className="mesgs">
                  {/* Chat-Box */}
                  <h1>Chat {room}</h1>
                  <div className="msg_history" >
                     <MessageList messages={messages} name={name}/>
                  </div>
                  {/* Fin Chat-Box */}
                  <div className="type_msg">
                     <form>
                        <div className="input_msg_write">
                           <input
                              type="text"
                              className="write_msg"
                              placeholder="Escribe tu mensaje aqui..."
                              onChange={({ target: { value } }) => setMessage(value)}
                              onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}
                           />
                           <button className="msg_send_btn" onClick={e => sendMessage(e)}><i className="fa fa-paper-plane-o"
                              aria-hidden="true"></i></button>
                        </div>
                     </form>
                  </div>
               </div>
               {/* Fin Mensajes - chat */}
            </div>
         </div>
      </div>
   )
}

export default Chat