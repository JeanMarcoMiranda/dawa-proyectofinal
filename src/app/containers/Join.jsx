import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

const Join = (props) => {
   
   const { user } = props
   const [sala, setSala] = useState('')

   const inputChange = (event) => {
      setSala(event.target.value)
   }

   return (
      <div>
         <h1>JOIN</h1>
         <div>
            <h3>Ingrese su Nombre:</h3>
            <input placeholder="Ingresa la sala" type="text" onChange={inputChange}/>
            <Link 
               onClick={e => !sala ? event.preventDefault() : null} 
               to={`/chat?name=${user.nombre}&room=${sala}`}
            >
               <button type="submit">Puto el que presione este boton</button>
            </Link>
         </div>
      </div>
   )
}

const mapStateToProps = ({user}) => {
   return {
      user
   }
}

export default connect(mapStateToProps, null)(Join)