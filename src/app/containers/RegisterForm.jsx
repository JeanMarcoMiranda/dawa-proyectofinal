import React, {useState} from 'react'
import axios from 'axios'
import { useForm } from 'react-hook-form';

const RegisterForm = (props) => {

  const {register, handleSubmit, errors, setError, clearError} = useForm()

  /* State Hooks Variables */
  const [password, setPassword] = useState('')


  const samePassword = (pass, passV) => {
    if (pass === passV){
      return true
    }else{
      return false
    }
  }
  

  const onSubmit = data => {
    const {name, email, password, passwordValidation : pV} = data
    if(samePassword(password, pV)){
      const data = {
        nombre: name,
        email: email,
        password: password,
      }
  
      axios.post('/api/users/', data)
        .then(res => {
          console.log(res)    
        })
        .catch(error => console.log(error))
      props.history.push("/list")
    }
  }


  return(
    <div className="container align-self-baseline">     
      <div className="card w-75 mx-auto">
        <h3 className="card-title text-center my-3">
          Registro de Usuario
        </h3>
        <div className="card-body p-4">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="mb-4">
              <label><h5>Nombre</h5></label>
              <input 
                type="text"
                name="name"
                className={`w-100 py-3 px-2 border`}
                ref={register({required: true, maxLength: 50})}/>
              <span
                role="alert"
                id="error-name-required"
                style={{
                  display: errors.name && errors.name.type === "required"
                    ? "block"
                    : "none",
                  color: '#EF7676'
                }}
              >
                El nombre del usuario es necesario
              </span>
              <span
                role="alert"
                id="error-email-required"
                style={{
                  display: errors.name && errors.name.type === "maxLength"
                    ? "block"
                    : "none",
                  color: '#EF7676'
                }}
              >
                El nombre del usuario debe ser menor a 50 caracteres.
              </span>
            </div>
            <div className="mb-4">
              <label><h5>Correo Electronico</h5></label>
              <input
                type="text"
                name="email" 
                className="w-100 py-3 px-2 border"
                ref={register({required: true, pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/})}/>
              <span
                role="alert"
                id="error-email-required"
                style={{
                  display: errors.email && errors.email.type === "required"
                    ? "block"
                    : "none",
                  color: '#EF7676'
                }}
              >
                El campo email es necesario
              </span>
              <span
                role="alert"
                style={{
                  display: errors.email && errors.email.type === "pattern"
                    ? "block"
                    : "none",
                  color: '#EF7676'
                }}
              >
                Email ingresado invalido, pruebe con otro.
              </span>
            </div>
            <div className="row mb-4">
              <div className="col-6">
                <div>
                  <label><h5>Contraseña</h5></label>
                  <input 
                    type="password"
                    name="password" 
                    className="w-100 py-3 px-2 border"
                    onChange={e => setPassword(e.target.value)}
                    ref={register({required: true, minLength: 8,})}/>
                  <span
                    role="alert"
                    id="error-password-required"
                    style={{
                      display: errors.password && errors.password.type === "required"
                        ? "block"
                        : "none",
                      color: '#EF7676'
                    }}
                  >
                    La contraseña es necesaria
                  </span>
                  <span
                    role="alert"
                    id="error-email-required"
                    style={{
                      display: errors.password && errors.password.type === "minLength"
                        ? "block"
                        : "none",
                      color: '#EF7676'
                    }}
                  >
                    Este campo debe tener un minimo de 8 caracteres.
                  </span>
                </div>
              </div>
              <div className="col-6">
                <div>
                  <label><h5>Validación de Contraseña</h5></label>
                  <input
                    type="password"
                    name="passwordValidation" 
                    className="w-100 py-3 px-2 border"
                    onChange={ e => {
                      const value = e.target.value
                      if(value === password || value.length === 0) return clearError("passwordV")
                      setError("passwordV", "notValidPass", "Este campo debe ser igual a Contraseña")
                    }} 
                    ref={register({required: true})}/>
                  <span
                    role="alert"
                    id="error-passwordValidation-required"
                    style={{
                      display: errors.passwordValidation && errors.passwordValidation.type === "required"
                        ? "block"
                        : "none",
                      color: '#EF7676'
                    }}
                  >
                    Este campo es necesario
                  </span>
                  <span
                    role="alert"
                    id="error-email-required"
                    style={{
                      display: errors.passwordV && errors.passwordV.type === "notValidPass"
                        ? "block"
                        : "none",
                      color: '#EF7676'
                    }}
                  >
                    {errors.passwordV && errors.passwordV.message}
                  </span>
                </div>
              </div>
            </div>
            <div className="mt-4">
              <button type="submit" className="btn btn-success w-100 p-3">
                <h5 className="my-auto">
                  Enviar
                </h5>    
              </button>
            </div>
          </form>
        </div>
        
        <div className="card-footer text-muted">
            <div className="d-flex justify-content-center my-2">
              <h5>¿Ya tienes una cuenta?</h5>
            </div>
            <div>
              <button type="submit" className="btn btn-outline-primary py-3 w-100 mb-3">
                <h5 className="my-auto">
                  Inicia tu sesión
                </h5>
              </button>
            </div>
        </div>

      </div>
    </div>
  )
}

export default RegisterForm