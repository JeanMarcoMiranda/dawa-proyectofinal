import React, { useState, useEffect } from 'react'
import { useParams, Redirect } from 'react-router-dom'
import axios from 'axios'
import { setUserLoged } from '../actions'
import { connect } from 'react-redux'


const UpdateForm = (props) => {

  const { id } = useParams()

  /* State Hooks Variables */
  const [userName, setUserName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [image, setImage] = useState('/user.png')
  const [imageToStore, setImageToStore] = useState()


  useEffect(() => {
    axios.get('/api/users/')
      .then(res => {
        const data = res.data.usuarios
        const currentUser = data.filter(user => user._id === id)
        const { nombre, email: e, password: pass, img : image } = currentUser[0]
        console.log(currentUser[0])
        setUserName(nombre)
        setEmail(e)
        setPassword(pass)
        setConfirmPassword(pass)
        setImage(`/${image}`)
      })
  }, [])

  const updateUser = (e) => {
    e.preventDefault()

    const formData = new FormData()
    formData.append('nombre', userName)
    formData.append('email', email)
    formData.append('password', password)
    formData.append('userImage', imageToStore)

    axios.put(`/api/users/${id}/`, formData)
      .then( res => {
        console.log("mi res", res.data);
        props.setUserLoged(res.data.newUserData)
        props.history.push("/list")
      })
      .catch(error => console.log(error));

  }

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      setImageToStore(event.target.files[0])
      reader.onload = (e) => {
        setImage(e.target.result);
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }


  return (

    <div className="container align-self-baseline h-75">
      <form onSubmit={updateUser}>
        <div className="card mb-3 rounded">
          <div className="row no-gutters">
            <div className="col-md-4 p-4 pt-5 d-flex flex-column justify-content-center align-items-center">
              <img src={image} className="border border-secondary" style={{borderRadius: 110}} width="220" height="220"/>
              <div className="mt-5">
                <label className="d-block h5 text-center">Imagen Usuario</label>
                <input
                  type="file"
                  className="w-100 p-3"
                  onChange={onImageChange}
                />
              </div>
            </div>
            <div className="col-md-8 p-4 bg-light" style={{borderLeftWidth:3, borderLeftColor: 'grey', borderLeftStyle: 'solid'}}>
              <h1 className="card-title text-center">
                Editar Usuario
              </h1>
              <div className="card-body p-4">
                <div className="mb-4">
                  <label><h5>Nombre</h5></label>
                  <input
                    type="text"
                    className={`w-100 p-3`}
                    onChange={(e) => setUserName(e.target.value)}
                    value={userName} />
                  <small />Ingrese un nombre que sea menor a 50 letras
                </div>
                <div className="mb-4">
                  <label><h5>Correo Electronico</h5></label>
                  <input
                    type="email"
                    className="w-100 p-3"
                    onChange={(e) => setEmail(e.target.value)}
                    value={email} />
                  <small />example@example.com
                    </div>
                <div className="row mb-4">
                  <div className="col-6">
                    <div>
                      <label><h5>Contraseña</h5></label>
                      <input
                        type="password"
                        className="w-100 p-3"
                        onChange={(e) => setPassword(e.target.value)}
                        value={password} />
                      <small />Su informacion no será compartida con nadie
                        </div>
                  </div>
                  <div className="col-6">
                    <div>
                      <label><h5>Validación de Contraseña</h5></label>
                      <input
                        type="password"
                        className="w-100 p-3"
                        onChange={(e) => setConfirmPassword(e.target.value)}
                        value={confirmPassword}/>
                      <small />Su informacion no será compartida con nadie
                    </div>
                  </div>
                </div>
                <div className="mt-4">
                  <button type="submit" className="btn btn-success w-100 p-3">
                    <h5 className="my-auto">
                      Enviar
                        </h5>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>

  )
}


const mapActionsToProps = {
  setUserLoged
}

export default connect(null, mapActionsToProps)(UpdateForm)