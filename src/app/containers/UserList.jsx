import React, { useEffect, useState } from 'react'
import axios from 'axios'


import {
  Link
} from 'react-router-dom'


const Lista = () => {
  
  const [userList, setUserList] = useState([])

  useEffect(() => {
    axios.get(`/api/users/`)
      .then(res => {
        setUserList(res.data.usuarios)
      })
  }, [])

  const deleteUser = (id) => {
    axios.delete(`/api/users/${id}`)
    .then(res => {
      const arrayUpdated = userList.filter(user => user._id != id)
      setUserList(arrayUpdated)
    }).catch(error => console.log(error))
  }

  return (
    <div className="container-fluid">
      <table className="table table-striped table-dark">
        <thead>
          <tr className="text-center">
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Correo</th>
            <th scope="col">Estado</th>
            <th scope="col">Rol</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {
            userList.map((user, index) => (
              <tr className="text-center" key={index}>
                <th>{index + 1}</th>
                <td>{user.nombre}</td>
                <td>{user.email}</td>
                <td>{(user.estado) ? "Activo" : "Inactivo"}</td>
                <td>{user.role}</td>
                <td>
                  <div className="my-2 my-lg-0">
                    <Link to={`/update/${user._id}`}>
                      <button 
                        className="btn btn-outline-warning my-2 my-sm-0"
                      >
                        Editar
                      </button>
                    </Link>                   
                    <button 
                      onClick={() => deleteUser(user._id)}
                      className="btn btn-outline-danger my-2 my-sm-0 ml-2" >
                      Eliminar
                    </button>
                  </div>
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>

      
    </div>
  )
}

export default Lista