import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import axios from 'axios'

//Reducers
import reducer from './reducers'

//My Files
import App from './App'

const initialState = {
  "user": {},
  "token": localStorage.getItem('token') || "",
  "userList": []
}


const store = createStore(reducer, initialState)


ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>, 
  document.getElementById('app'))