import React from 'react'
import { connect } from 'react-redux'
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom'


import Login from './containers/LoginForm.jsx'
import Lista from './containers/UserList.jsx'
import RegisterForm from './containers/RegisterForm.jsx'
import UpdateForm from './containers/UpdateForm.jsx'
import Layout from './components/Layout.jsx'
import Join from './containers/Join.jsx'
import Chat from './containers/Chat.jsx'

const App = ({token}) => {

  console.log("El estado actual es ", token)

  return (
    <BrowserRouter>
      <Layout token={token} >
        <Switch>
          <Route
            path="/login"
            component={Login}
          />
          <Route exacts
            path="/register"
            component={RegisterForm}
          />
          <Route
            path="/update/:id"
            component={UpdateForm}
          />
          <Route
            path="/chat"
            component={Chat}
          />
          <Route path="/"
            render={(props) => {
              let mostrar
              (token)
                ? mostrar = <Join />
                : mostrar = <Login {...props}/>
              return (mostrar)
            }
            }
          />
        </Switch>
      </Layout>
    </BrowserRouter>
  )
}

const mapStateToProps = state => {
  return {
    token : state.token,
  }
}

export default connect(mapStateToProps, null)(App)

