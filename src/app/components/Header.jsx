import React, {useEffect} from 'react'
import { connect } from 'react-redux'
import { setLogedIn, setUserLoged } from '../actions'
import axios from 'axios'
import { Link } from 'react-router-dom'

const Header = (props) => {

  const {user, token} = props
  const {setUserLoged, setLogedIn} = props
  const appName = user.nombre || "App"
  console.log(user.img)
  const appImage = (user.img) ? `/${user.img}` : "/user.png"

  useEffect(() => {
    axios.get('/api/users', {
      headers: {
        token : token
      }
    })
    .then(res => {
      setUserLoged(res.data.usuarios)
    })
    .catch(err => console.log("error"))
  }, [])

  const logout = () => {
    setLogedIn("")
    setUserLoged({})
    localStorage.removeItem('token')
  }

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark w-100 align-self-start d-flex justify-content-between">
        <div className="d-flex align-items-center">
          <img style={{ borderRadius: 100 }} src={appImage} alt="Default Image User" width="45" height="45"/>
          <Link to="/" className="navbar-brand ml-2" >{appName}</Link>
        </div>
        <div>
          {
            (!token)
              ? (<div className="">
                <Link to="/login">
                  <button className="btn btn-outline-success my-2 my-sm-0">
                    Log in
                      </button>
                </Link>
                <Link to="/register">
                  <button className="btn btn-outline-primary my-2 my-sm-0 ml-2" >
                    Register
                      </button>
                </Link>
              </div>)
              : (<div className="my-2 my-lg-0">
                <Link to="/update/5ef689180bb0a716269dc053">
                  <button className="btn btn-outline-secondary my-2 my-sm-0">
                    Profile
                  </button>
                </Link>
                <Link to="/">
                  <button className="btn btn-outline-danger my-2 my-sm-0"
                    onClick={logout}
                  >
                    Log out
                  </button>
                </Link>
              </div>)
          }
        </div>
      </nav>
    </>
  )
}

const mapStateToProps = ({token, user}) => {
  return {
    token,
    user
  }
}

const mapDispatchToProsps = {
  setLogedIn,
  setUserLoged
}

export default connect(mapStateToProps, mapDispatchToProsps)(Header)