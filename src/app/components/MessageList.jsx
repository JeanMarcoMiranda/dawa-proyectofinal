import React from 'react'

const CurrentUserMenssage = ({ message, i}) => {
  return (
     <div key={i}>
        <div className="outgoing_msg">
           <div className="sent_msg">
              <div>
                 <p>{message.mensaje}</p>
                 <span className="time_date"> {message.nombre} </span>
              </div>
           </div>
        </div>
     </div>
  )
}

const OthersMessages = ({message, i}) => {
  return (
     <div key={i}>
        <div className="incoming_msg">
           <div className="received_msg">
              <div className="received_withd_msg">
                 <p>{message.mensaje}</p>
                 <span className="time_date"> {message.nombre} </span>
              </div>
           </div>
        </div>
     </div>
  )
}

const MessageList = ({messages, name}) => {
  return (
     messages.map((message, index) => {

        return (name === message.nombre)
           ? <CurrentUserMenssage message={message} i={index}/>
           : <OthersMessages message={message} i={index}/>
                    
     })
  )
}

export default MessageList