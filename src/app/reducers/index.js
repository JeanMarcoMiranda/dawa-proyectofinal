const reducer = (state, action) => {
  switch(action.type) {
    case 'SET_LOGEDIN':
      return {
        ...state,
        token: action.payload
      };
    case 'SET_USERLOGED':
      return {
        ...state,
        user: action.payload
      };
    default:
      return state;
  }
}

export default reducer;